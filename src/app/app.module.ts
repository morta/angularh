import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import {RouterModule,Routes} from '@angular/router';
import {AngularFireModule} from 'angularfire2';

import { AppComponent } from './app.component';
import { UsersComponent } from './users/users.component';
import { DemoComponent } from './demo/demo.component';
import { PostsComponent } from './posts/posts.component';
import { PostsService } from './posts/posts.Service';
import { PostComponent } from './post/post.component';
import { SpinnerComponent } from './shared/spinner/spinner.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { PostFormComponent } from './post-form/post-form.component';
import { UsersService } from './users/users.service';
import { UserComponent } from './user/user.component';
import { UserFormComponent } from './user-form/user-form.component';

export const firebaseConfig = {
    apiKey: "AIzaSyACIwB03n0t4peNt5Kp1Tf6-D1r1GKxLZ0",
    authDomain: "homeworkproject-6b16d.firebaseapp.com",
    databaseURL: "https://homeworkproject-6b16d.firebaseio.com",
    storageBucket: "homeworkproject-6b16d.appspot.com",
    messagingSenderId: "890165806290"
}

const appRoutes: Routes = [
  {path:'users', component: UsersComponent},
  {path:'posts', component: PostsComponent},
  {path:'', component:UsersComponent},
  {path:'**', component:PageNotFoundComponent}
    ]

@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    DemoComponent,
    PostsComponent,
    PostComponent,
    SpinnerComponent,
    PageNotFoundComponent,
    PostFormComponent,
    UserComponent,
    UserFormComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule,
    RouterModule.forRoot(appRoutes),
    AngularFireModule.initializeApp(firebaseConfig)
  ],
  providers: [PostsService, UsersService],
  bootstrap: [AppComponent]
})
export class AppModule { }
