import { Component, OnInit } from '@angular/core';
import { UsersService } from './users.service';

@Component({
  selector: 'jce-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
    users; 
   // = [
   // {name:'John',email:'john@gmail.com'},
   // {name:'Jack',email:'jack@gmail.com'},
   // {name:'Alice',email:'alice@yahoo.com'}
   // ]

   currentUser;

   select(user){
    this.currentUser = user;
   }
  constructor(private _usersService:UsersService) { }

  addUser(user){
    this.users.push(user)
  }
  
  ngOnInit() {
    this._usersService.getUsers().subscribe(usersData => this.users = usersData);
  }

}
