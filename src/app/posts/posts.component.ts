import { Component, OnInit } from '@angular/core';
import { PostsService } from './posts.Service';

@Component({
  selector: 'jce-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {
isLoading:Boolean = true;
posts;
currentPost;


select(post){
  this.currentPost = post;
}

  constructor(private _postsService:PostsService) { }

  addPost(post){
    //this.posts.push(post)
    this._postsService.addPosts(post);
  }


  deletePost(post) {
    //this.posts.splice(
      //this.posts.indexOf(post),1)
     this._postsService.deletePost(post);    
          }
  editPost(originalAndEdited){
    this.posts.splice(
      this.posts.indexOf(originalAndEdited[0]),1,originalAndEdited[1]
      )  
  }

  updatePost(post){
  this._postsService.updatePost(post);
  }

 ngOnInit() {
  this._postsService.getPosts().subscribe
  (postsData => {this.posts=postsData; 
    this.isLoading=false});
  }

}
