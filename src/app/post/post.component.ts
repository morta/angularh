import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import {Post} from './post';

@Component({
  selector: 'jce-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css'],
  inputs:['post']
})
export class PostComponent implements OnInit {
  post:Post;
 @Output() deleteEvent = new EventEmitter<Post>();
 @Output() editEvent = new EventEmitter<Post>();

  tryPost:Post = {title:null, body:null}
  isEdit:Boolean = false;
  editButtonText = 'Edit';
  constructor() { }
 
  sendDelete(){
    this.deleteEvent.emit(this.post);
  }

  sendEdit() {
    this.isEdit = false;
    this.post.title = this.tryPost.title;
    this.post.body = this.tryPost.body;
    this.editButtonText = 'Edit';

  }

  toggleEdit () {
    this.isEdit = !this.isEdit;
    this.isEdit ? this.editButtonText = 'Save' : this.editButtonText ='Edit';
   // if(this.isEdit){
   //   this.tryPost.title = this.post.title;
   //   this.tryPost.body = this.post.body;

 //   } else {
   //    let originalAndNew = [];
    //  originalAndNew.push(this.tryPost,this.post);
    //   this.editEvent.emit(originalAndNew);
   // }
   if(!this.isEdit){
    this.editEvent.emit(this.post);
   } 
  }

  ngOnInit() {
  }

}
